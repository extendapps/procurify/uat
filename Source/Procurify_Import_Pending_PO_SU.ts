/**
 *
 * @copyright 2018 Darren Hill Consulting Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType Suitelet
 */

import {EntryPoints} from 'N/types';
import * as task from 'N/task';
import * as https from 'N/https';
import * as runtime from 'N/runtime';

export let onRequest: EntryPoints.Suitelet.onRequest = (context: EntryPoints.Suitelet.onRequestContext) => {
    let mapReduceScriptTask: task.MapReduceScriptTask = <task.MapReduceScriptTask>task.create({taskType: task.TaskType.MAP_REDUCE});
    mapReduceScriptTask.scriptId = 'customscript_procurify_object_importer';
    mapReduceScriptTask.deploymentId = null; // Setting this to null forces Netsuite to select the next available 'idle' deployment
    try {
        mapReduceScriptTask.submit();
        context.response.sendRedirect({
            type: https.RedirectType.TASK_LINK,
            identifier: 'LIST_MAPREDUCESCRIPTSTATUS'
        });
    } catch (e) {
        // Redirect back to an appropoirate spot? (Reload this script?
        context.response.sendRedirect({
            type: https.RedirectType.SUITELET,
            id: runtime.getCurrentScript().id,
            identifier: runtime.getCurrentScript().deploymentId,
            parameters: {
                errorMessage: e.message
            }
        });
    }
};