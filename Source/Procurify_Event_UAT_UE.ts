/**
 * Module Description...
 *
 * @file Shopify_StoreFront_UE.js
 * @copyright 2018 Darren Hill Consulting Inc.
 * @author Darren Hill darren@darrenhillconsulting.ca

 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType UserEventScript
 */

import {EntryPoints} from 'N/types';
import * as search from 'N/search';
import * as record from 'N/record';
import * as log from 'N/log';

export let beforeSubmit: EntryPoints.UserEvent.beforeSubmit = (context: EntryPoints.UserEvent.beforeSubmitContext) => {
    if (context.type === context.UserEventType.DELETE) {
        // Delete any related transactions
        search.create({
            type: search.Type.TRANSACTION,
            filters: [
                ['mainline', 'is', 'T'],
                'AND',
                ['custbody_procurify_event', 'anyof', context.newRecord.id]
            ]
        }).run().each(result => {
            try {
                record.delete({
                    type: <string>result.recordType,
                    id: result.id
                });
            } catch (e) {
                log.error(`Error while deleteing Procurify Event: ${context.newRecord.id}`, `${result.recordType} : ${result.id} - ${e}`);
            }
            return true;
        });

        // Update the Procurify back to 'pending'
        search.create({
            type: 'customrecord_procurify_event',
            filters: [
                ['internalid', 'anyof', context.newRecord.id]
            ],
            columns: [
                'custrecord_procurify_event_type',
                'custrecord_procurify_event_config',
                'custrecord_procurify_event_data'
            ]
        }).run().each(result => {
            // getObjectTypeDetails({
            //     objectTypeId: +result.getValue(result.columns[0]),
            //     Found: objectTypeDetails => {
            //         let handlerDetails: HandlerDetails = getHandler(objectTypeDetails.scriptId);
            //         let configurationId: number = +result.getValue(result.columns[1]);
            //         let data: BaseObject = JSON.parse(<string>result.getValue(result.columns[2]));
            //         let procurify: Procurify = new Procurify(configurationId);
            //
            //         Procurify_Event.flag({
            //             procurify: procurify,
            //             handlerDetails: handlerDetails,
            //             context: context,
            //             object_id: data.id,
            //             status: ObjectMapStatus.pending,
            //             message: 'Resetting'
            //         });
            //     }
            // });
            return false;
        });
    }
};