/**
 *
 * @copyright 2018 Darren Hill Consulting Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType Suitelet
 */

import {EntryPoints} from 'N/types';
// import {Procurify} from '../Procurify';
import * as log from 'N/log';
// import {ObjectMapStatus} from '../Objects/ObjectMap';
// import {Procurify_Configuration} from '../Procurify_Configuration';

declare let Procurify_Configuration;
declare let ObjectMapStatus;
declare let Procurify;

// noinspection JSUnusedLocalSymbols
export let onRequest: EntryPoints.Suitelet.onRequest = (context: EntryPoints.Suitelet.onRequestContext) => {
    // @ts-ignore
    let procurify: Procurify = new Procurify(-1);
    let statuses: string[] = ['synced', 'error'];

    statuses.forEach(status => {
        procurify.getPurchaseOrders({
            status: <'synced' | 'error'>status,
            OK: purchaseOrders => {
                purchaseOrders.forEach(purchaseOrder => {
                    log.audit('purchaseOrder', purchaseOrder);
                    procurify.getContentType({
                        model: 'po',
                        OK: contentType => {
                            log.audit('contentType', contentType);
                            procurify.upsertObjectMap({
                                objectMap: {
                                    object_id: purchaseOrder.id,
                                    content_type: contentType.id,
                                    external_id: '',
                                    status: ObjectMapStatus.pending
                                },
                                Created: objectMap => {
                                    log.audit('upsertObjectMap', objectMap);
                                },
                                Failed: clientResponse => {
                                    log.error('upsertObjectMap', clientResponse);
                                }
                            });
                        }
                    });
                });
            },
            Failed: clientResponse => {
                log.error('getPurchaseOrders', clientResponse);
            }
        });
    });

    Procurify_Configuration.navigateToManager(context.response);
};